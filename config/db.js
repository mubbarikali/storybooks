const mangoose = require('mongoose');

const connectDB = async() => {
    try {
        const conn = await mangoose.connect(process.env.MONGO_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
        })

        console.log(`MongoDB Connected: ${conn.connection.host}`)
    } catch (error) {
        console.error(err)
        process.exit(1)
    }
}

module.exports = connectDB